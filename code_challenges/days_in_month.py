"""Days in a month"""


def is_leap(l_year):
    """Return true on leap year"""
    if l_year % 4 == 0:
        if l_year % 100 == 0:
            if l_year % 400 == 0:
                return True
            return False
        return True
    return False


def days_in_month(f_year, f_month):
    """Return the number of days in a month """
    month_days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    if f_month == 2 and is_leap(f_year):
        return 29
    return month_days[month - 1]


# Do NOT change any of the code below
year = int(input("Enter a year: "))
month = int(input("Enter a month: "))
days = days_in_month(year, month)
print(days)
