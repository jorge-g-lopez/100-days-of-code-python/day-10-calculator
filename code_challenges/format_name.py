"""Format Name"""


def format_name(first_name, last_name):
    """Return the full name with title format"""
    full_name = first_name + " " + last_name
    return full_name.title()


print(format_name("jOrGe", "LoPeZ"))
