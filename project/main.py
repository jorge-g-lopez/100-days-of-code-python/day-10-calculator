"""Calculator"""

from art import LOGO


def add(n1, n2):
    """Return n1 + n2"""
    return n1 + n2


def substract(n1, n2):
    """Return n1 - n2"""
    return n1 - n2


def multiply(n1, n2):
    """Return n1 * n2"""
    return n1 * n2


def divide(n1, n2):
    """Return n1 / n2"""
    return n1 / n2


OPERATIONS = {"+": add, "-": substract, "*": multiply, "/": divide}

# Using recursion as instructed


def calculator():
    """Function to perform operations with two numbers"""
    print(LOGO)

    calculate = "y"
    num1 = float(input("What's the first number?: "))

    for operation in OPERATIONS:
        print(operation)

    while calculate == "y":
        symbol = input("Pick an operation: ")
        num2 = float(input("What's the next number?: "))

        answer = OPERATIONS[symbol](num1, num2)

        print(f"{num1} {symbol} {num2} = {answer}")
        calculate = input(
            f"Type 'y' to continue calculating with {answer}, or type 'n' to exit: "
        ).lower()
        num1 = answer
    calculator()


calculator()
